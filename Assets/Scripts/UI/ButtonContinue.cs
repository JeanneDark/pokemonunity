﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;


public class ButtonContinue : MonoBehaviour {

	void Start () {
		Button b = GetComponent<Button>();
		b.onClick.AddListener (() => {
			SceneManager.LoadScene ("Main");
		});
			
	}

}
