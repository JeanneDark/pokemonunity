﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Menu : MonoBehaviour {

	void OnGUI() {
		if (GUI.Button (new Rect ((Screen.width / 2) - 75, (Screen.height / 2) - 50, 150, 100), "Start Game"))
			SceneManager.LoadScene ("StartMenu");
	}
}
