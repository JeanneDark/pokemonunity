﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent (typeof (AudioSource))]

public class Video : MonoBehaviour {

	public MovieTexture movie;

	void Start () {
		GetComponent<Renderer>().material.mainTexture = movie as MovieTexture;
		GetComponent<AudioSource>().clip = movie.audioClip;
		movie.loop = true;
		movie.Play ();
		GetComponent<AudioSource>().Play ();
	}

	void OnMouseDown() {
		movie.Stop ();
		SceneManager.LoadScene ("StartMenu");
	}

	void Update() {
		if (!movie.isPlaying) {
			SceneManager.LoadScene ("StartMenu");
		}
	}
}
