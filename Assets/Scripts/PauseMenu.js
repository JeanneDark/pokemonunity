﻿#pragma strict

var paused : boolean = false;
var menu : int = 0;

function OnGUI () {

	var other : SaveSystem;
	other = gameObject.Find("Player").GetComponent(SaveSystem);

	if (paused) {
		if(GUI.Button(Rect(10,70,100,30),"Index")) menu = 1;
		if(GUI.Button(Rect(10,100,100,30),"Pokemon")) menu = 2;
		if(GUI.Button(Rect(10,130,100,30),"Bag")) menu = 3;
		if(GUI.Button(Rect(10,160,100,30),"Character")) menu = 4;
		if(GUI.Button(Rect(10,190,100,30),"Save")) menu = 5;
		if(GUI.Button(Rect(10,220,100,30),"Options")) menu = 6;
		if(GUI.Button(Rect(10,250,100,30),"Exit")) Application.Quit();

		switch (menu) { //case 0, "default" = no menu selected.
			case 1:
				GUI.Box(Rect(110, 70, 200, 300), "INDEX");
				break;
			case 2:
				GUI.Box(Rect(110, 70, 200, 300), "Pokemon");
				break;
			case 3:
				GUI.Box(Rect(110, 70, 200, 300), "Bag");
				break;
			case 4:
				GUI.Box(Rect(110, 70, 200, 300), "CHARACTER");
				break;
			case 5:
				GUI.Box(Rect(110, 70, 200, 300), "Save");
				other.Save();
				break;
			case 6:
				GUI.Box(Rect(110, 70, 200, 300), "OPTIONS");
				break;
			default:
				Debug.Log("No Menu Selected");
				break;
		}
	}
}

function Start () {

}

function Update () {
	if (Input.GetKeyDown("p")) {
		paused = !paused;
		Time.timeScale = 0;
	}
}