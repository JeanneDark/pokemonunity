﻿#pragma strict

class Monster {

	var name : String;
	var image : Texture2D;
	var rarity : Rarity;
	var regionLocated : String;
	var type : Type;
	var baseHp : float;
	var curHp : float;
	var baseAtk : float;
	var curAtk : float;
	var baseDef : float;
	var curDef : float;
	var attacks : Attack[];
	// var speed : float;

}

enum Type {
	fire,
	water,
	grass,
	psychic,
	electric
}

enum Rarity {
	common,
	rare
}