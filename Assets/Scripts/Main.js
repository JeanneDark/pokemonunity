﻿#pragma strict
import System.Collections.Generic;

// All Pokemon
var pokemon : Monster[];

// Pokemon we encountered
var enemyPokemon : Monster;

// Equipped Pokemon (are current Pokemon fighting)
var pokemonEquipped : Monster;

// Turn 0 = Player, 1 = Enemy
var turn : int;

// access other scripts
var other : PlayerStats;
var pokemonAttacks : PokemonAttacks;

function Awake() {
	other = gameObject.Find("Player").GetComponent(PlayerStats);
	pokemonAttacks = gameObject.Find("Player").GetComponent(PokemonAttacks);
}

function Start () {
	copyPokemon();
}

function Update () {

	if (other.isInCombat) {
		if (pokemonEquipped.curHp <= 0) {
			Debug.Log("Our Pokemon died");
			other.exitCombat();
		}

		if (enemyPokemon.curHp <= 0) {
			Debug.Log("Enemy Pokemon died");
			other.exitCombat();
		}

	}

}

function OnGUI () {

	var other : PlayerStats;
	other = gameObject.Find("Player").GetComponent(PlayerStats);

	// Display Pokemon during battle
	if (other.isInCombat) {

		GUI.Label(Rect(50, 100, 200, 100), "" + enemyPokemon.name);
		GUI.Label(Rect(50, 110, 200, 100), "" + enemyPokemon.baseHp + " / " + enemyPokemon.curHp);
		GUI.DrawTexture(Rect(70, 100, 128, 128), enemyPokemon.image);

		GUI.Label(Rect(Screen.width / 2, 100, 200, 100), "" + pokemonEquipped.name);
		GUI.Label(Rect(Screen.width / 2, 110, 200, 100), "" + pokemonEquipped.baseHp + " / " + pokemonEquipped.curHp);
		GUI.DrawTexture(Rect(Screen.width / 2 + 40, 150, 128, 128), pokemonEquipped.image);

		for (var i = 0; i < pokemonEquipped.attacks.Length && i < 4; i++) {
			if (turn == 0) {
				if (GUI.Button(Rect(150, 100 + (i * 50), 100, 30), "" + pokemonEquipped.attacks[i].name)) {
					Debug.Log("Used " + pokemonEquipped.attacks[i].name + " Attack");
					pokemonAttacks.useAbility("" + pokemonEquipped.attacks[i].name, turn);
					playerAttacked();
				}
			}
		}
	}

}

function randomizePokemon () {

	var other : PlayerStats;
	other = gameObject.Find("Player").GetComponent(PlayerStats);

	var tempPokemon : List.<Monster> = new List.<Monster>();
	var randomNum : int = Random.Range(0, 100);

	if (randomNum == 20) {
		for (var i = 0; i < pokemon.Length; i++) {
			if (pokemon[i].rarity == Rarity.rare
					&& pokemon[i].regionLocated == other.region) {

				tempPokemon.Add(pokemon[i]);
			}
		}
	} else {
		for (var j = 0; j < pokemon.Length; j++) {
			if (pokemon[j].rarity == Rarity.common
					&& pokemon[j].regionLocated == other.region) {

				tempPokemon.Add(pokemon[j]);
			}
		}
	}

	var newRandom = Random.Range(0, tempPokemon.Count);
	enemyPokemon = tempPokemon[newRandom];

}

function playerAttacked() {
	Debug.Log("Player Attacked");
	turn = 1;
	enemyAttacked();
}

function enemyAttacked() {

	yield WaitForSeconds(2);

	if (other.isInCombat) {
		Debug.Log("Enemy Attacked");
		var randomAttack = Random.Range(0, 4);
		pokemonAttacks.useAbility("" + enemyPokemon.attacks[randomAttack].name, turn);
	}

	yield WaitForSeconds(2);
	turn = 0;
}

function copyPokemon() {
	var equipPokemon = new Monster();
	equipPokemon = pokemon[0];
	pokemonEquipped.name = equipPokemon.name;
	pokemonEquipped.image = equipPokemon.image;
	pokemonEquipped.rarity = equipPokemon.rarity;
	pokemonEquipped.baseHp = equipPokemon.baseHp;
	pokemonEquipped.curHp = equipPokemon.curHp;
	pokemonEquipped.baseAtk = equipPokemon.baseAtk;
	pokemonEquipped.curAtk = equipPokemon.curAtk;
	pokemonEquipped.baseDef = equipPokemon.baseDef;
	pokemonEquipped.curDef = equipPokemon.curDef;

}