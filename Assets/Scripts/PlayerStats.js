﻿#pragma strict

// Moving Stats
var STATE_UP : int = 0;
var STATE_DOWN : int = 1;
var STATE_LEFT : int = 2;
var STATE_RIGHT : int = 3;

var directionFacing : int;

var startPoint : Vector3;
var endPoint : Vector3;
var speed : float = 4.0;
private var increment : float;
var isMoving : boolean;
var disableMovement : boolean;
var movingDelta : float = 0.1;
var isInCombat : boolean = false;

var _currentAnimationState : int;

// Counts the passed steps in tall grass
var walkCounter : int;
// Determines the limit for the walkCounter until the player
// is forced into a combat
var walkCounterLimit : int;

// Cameras
var MainCamera : GameObject;
var CombatCamera : GameObject;

// Teleport Locations
var teleportLoc : GameObject[];

// Regions
var region : String;

// Main Script
var main : Main;

var animator : Animator;

function Start () {
	MainCamera.SetActive(true);
	startPoint = transform.position;
	Debug.Log("transform.pos: " + transform.position);
	endPoint = transform.position;
	//define the animator attached to the player
	animator = GetComponent("Animator");
	walkCounterLimit = Random.Range(5, 15);

	region = "region1";
}

function Update () {

	if (Input.GetKeyDown("space") && !disableMovement) {
		talkToNPC();
	}

	if (increment <= 1 && isMoving == true) {
		increment += speed / 100;
		//Debug.Log("Moving");
	} else {
		isMoving = false;
		//Debug.Log("Stopped");
	}

	if (isMoving) {
		transform.position = Vector3.Lerp(startPoint, endPoint, increment);
		//Debug.Log("transform.pos: x: " + transform.position.x
		//			+ " y: " + transform.position.y
		//			+ " z: " + transform.position.z);
	}

	var disableMove : boolean = false;
	var hit : RaycastHit;
	var distanceToGround;

	if (!isInCombat && !disableMovement) {
		if (Input.GetKey("w") && isMoving == false) {
			// Debug.Log("--------------w");
			directionFacing = STATE_UP;

			if (Physics.Raycast(transform.position, Vector3.up, hit, 0.319)) {
				distanceToGround = hit.distance;
				// Debug.Log("Hit: " + distanceToGround
				//			+ " tag: " + hit.collider.gameObject.tag);

				if (hit.collider.gameObject.tag == "collision") {
					//Debug.Log("collision");
					disableMove = true;
				} else if (hit.collider.gameObject.tag == "tallGrass") {
					//Debug.Log("TallGrass");
				}
			}

			if (!disableMove) {
				calculateWalk();
				increment = 0;
				isMoving = true;
				startPoint = transform.position;
				endPoint = new Vector3(transform.position.x, transform.position.y + 0.319, transform.position.z);
				changeState(STATE_UP);
				changeIsMoving(true);
			}

		} else if (Input.GetKey("s") && isMoving == false) {
			// Debug.Log("--------------s");
			directionFacing = STATE_DOWN;

			if (Physics.Raycast(transform.position, Vector3.down, hit, 0.319)) {
				distanceToGround = hit.distance;
				// Debug.Log("Hit: " + distanceToGround
				//			+ " tag: " + hit.collider.gameObject.tag);

				if (hit.collider.gameObject.tag == "collision") {
					//Debug.Log("collision");
					disableMove = true;
				} else if (hit.collider.gameObject.tag == "tallGrass") {
					//Debug.Log("TallGrass");
				}
			}

			if (!disableMove) {
				calculateWalk();
				increment = 0;
				isMoving = true;
				startPoint = transform.position;
				endPoint = new Vector3(transform.position.x, transform.position.y - 0.319, transform.position.z);
				changeState(STATE_DOWN);
				changeIsMoving(true);
			}
		} else if (Input.GetKey("a") && isMoving == false) {
			// Debug.Log("--------------a");
			directionFacing = STATE_LEFT;

			if (Physics.Raycast(transform.position, Vector3.left, hit, 0.319)) {
				distanceToGround = hit.distance;
				// Debug.Log("Hit: " + distanceToGround
				//			+ "tag: " + hit.collider.gameObject.tag);

				if (hit.collider.gameObject.tag == "collision") {
					Debug.Log("collision");
					disableMove = true;
				} else if (hit.collider.gameObject.tag == "tallGrass") {
					Debug.Log("TallGrass");
				}
			}

			if (!disableMove) {
				calculateWalk();
				increment = 0;
				isMoving = true;
				startPoint = transform.position;
				endPoint = new Vector3(transform.position.x - 0.319, transform.position.y, transform.position.z);
				changeState(STATE_LEFT);
				changeIsMoving(true);
			}
		} else if (Input.GetKey("d") && isMoving == false) {
			// Debug.Log("--------------d");
			directionFacing = STATE_RIGHT;

			if (Physics.Raycast(transform.position, Vector3.right, hit, 0.319)) {
				distanceToGround = hit.distance;
				// Debug.Log("Hit: " + distanceToGround
				//			+ "tag: " + hit.collider.gameObject.tag);

				if (hit.collider.gameObject.tag == "collision") {
					Debug.Log("collision");
					disableMove = true;
				} else if (hit.collider.gameObject.tag == "tallGrass") {
					Debug.Log("TallGrass");
				}
			}

			if (!disableMove) {
				calculateWalk();
				increment = 0;
				isMoving = true;
				startPoint = transform.position;
				endPoint = new Vector3(transform.position.x + 0.319, transform.position.y, transform.position.z);
				changeState(STATE_RIGHT);
				changeIsMoving(true);
			}
		}
	}

}

//--------------------------------------
// Change the players animation state
//--------------------------------------
function changeState(state : int) {
	//Debug.Log("State: " + state);
	animator.SetInteger ("state", state);
	_currentAnimationState = state;
}

function changeIsMoving(moving : boolean) {
	animator.SetBool("isMoving", moving);
}

function talkToNPC() {

	if (directionFacing == 0) {
		var hit : RaycastHit;
		if (Physics.Raycast(transform.position, Vector3.up, hit, 0.319)) {
			//var distance = hit.distance;
			//Debug.Log("Hit distance: " + distance);

			if (hit.collider.gameObject.name == "npc") {
				hit.collider.SendMessage("talk");
			}

		}
	}

}

function calculateWalk() {

	// downwards raycast
	// if hit tall grass then check counter
	yield WaitForSeconds(0.3);

	var hit : RaycastHit;
	if (Physics.Raycast(transform.position, -Vector3.up, hit, 0.319)) {
		var distanceToGround = hit.distance;
		//Debug.Log("Hit: " + distanceToGround);

		if (hit.collider.gameObject.tag == "tallGrass") {
			//Debug.Log("TallGrass");
			walkCounter++;
		}
	}

	if (walkCounter >= walkCounterLimit) {
		walkCounterLimit = Random.Range(5, 10);
		walkCounter = 0;
		enterCombat();
	}

}

function enterCombat() {
	Debug.Log("Enter combat");

	main.randomizePokemon();

	MainCamera.SetActive(false);
	CombatCamera.SetActive(true);
	isInCombat = true;
}

function exitCombat() {
	MainCamera.SetActive(true);
	CombatCamera.SetActive(false);
	isInCombat = false;
}

function OnTriggerEnter(col : Collider) {

	Debug.Log("--------OnTriggerEnter: " + col.gameObject.tag);

	if (col.gameObject.tag == "caveEntrance1") {
		region = "region3";
		this.transform.position = teleportLoc[0].transform.position;
		this.transform.position.z -= 1;
	} else if (col.gameObject.tag == "caveExit1") {
		region = "region1";
		this.transform.position = teleportLoc[1].transform.position;
		this.transform.position.z -= 1;
	} else if (col.gameObject.tag == "region1") {
		region = "region1";
	} else if (col.gameObject.tag == "region2") {
		region = "region2";
	} else if (col.gameObject.tag == "region3") {
		region = "region3";
	}

}