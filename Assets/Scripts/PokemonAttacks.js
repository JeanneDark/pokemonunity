﻿#pragma strict

var main : Main;

function Awake() {
	main = gameObject.Find("Main").GetComponent.<Main>();
}

function Start () {

}

function Update () {

}

function useAbility(attackName : String, turn : int) {
	switch (attackName) {
		case "vine":
			vine(turn);
			break;
		case "smash":
			smash(turn);
			break;
		default:
			Debug.Log("none");
			break;
	}
}

function vine(turn : int) {
	Debug.Log("Attacked with Vine");
	calcDamage(5, turn);
}

function smash(turn : int) {
	Debug.Log("Attacked with smash");
	calcDamage(2, turn);
}

function calcDamage(damage : int, turn : int) {
	if (turn == 0) {
		main.enemyPokemon.curHp -= damage;
	} else {
		main.enemyPokemon.curHp -= damage;
	}
}