﻿#pragma strict

var uvAnimationTileX : int = 4;        //Here you can place the number of columns of your sheet.
var uvAnimationTileY : int = 4;        //Here you can place the number of rows of your sheet.
var framesPerSecond : float = 10.0f;

function Update () {
	var index : int = Time.time * framesPerSecond;
	index = index % (4 * 4);
	var size = Vector2(1.0 / uvAnimationTileX, 1.0 / uvAnimationTileY);
	var uIndex = index % uvAnimationTileX;
	var vIndex = index / uvAnimationTileX;
	var offset = Vector2(uIndex * size.x, 1.0 - size.y - vIndex * size.y);
	GetComponent.<Renderer>().material.SetTextureOffset("_MainTex", offset);
	GetComponent.<Renderer>().material.SetTextureScale("_MainTex", size);
}