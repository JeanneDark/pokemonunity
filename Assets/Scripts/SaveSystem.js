﻿#pragma strict

var player : GameObject;

function Start () {
	player = GameObject.Find("Player");
	Load();
}

function Load () {
	if (PlayerPrefs.GetFloat("playerX") != null) {
		player.transform.position.x = (PlayerPrefs.GetFloat("playerX"));
		player.transform.position.y = (PlayerPrefs.GetFloat("playerY"));
		player.transform.position.z = -1.0;
	}
}

function Save () {

	Debug.Log("Save");

	PlayerPrefs.SetFloat("playerX", player.transform.position.x);
	PlayerPrefs.SetFloat("playerY", player.transform.position.y);

}